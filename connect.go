package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/http"
	"strings"
	"time"
)

type LifecyclePayload struct {
	Key          string `json:"key"`
	ClientKey    string `json:"clientKey"`
	PublicKey    string `json:"publicKey"`
	SharedSecret string `json:"sharedSecret"`
	EventType    string `json:"eventType"`
}

const schema = `
create table if not exists lifecycle(
  key text,
  clientKey text not null,
  publicKey text,
  sharedSecret text not null,
  eventType text,
  time text
);`

type DBConn func() (*sql.DB, error)

var openDb DBConn

func InitConnectStorage(dbFunc DBConn) error {
	openDb = dbFunc
	db, err := openDb()
	if err != nil {
		return err
	}
	defer db.Close()

	// Initialise the tables if required
	_, err = db.Exec(schema)
	if err != nil {
		return err
	}

	return nil
}

func AddonInstalled(w http.ResponseWriter, r *http.Request) {
	db, err := openDb()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer db.Close()

	decoder := json.NewDecoder(r.Body)

	var lp LifecyclePayload
	err = decoder.Decode(&lp)

	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Store
	_, err = db.Exec("insert into lifecycle(key, clientKey, publicKey, sharedSecret, eventType, time) values (?, ?, ?, ?, ?, ?)", lp.Key, lp.ClientKey, lp.PublicKey, lp.SharedSecret, lp.EventType, time.Now().Format(time.RFC3339))

	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func lookupSecret(clientKey string) ([]byte, error) {
	db, err := openDb()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	row := db.QueryRow("select sharedSecret from lifecycle where clientKey = ?", clientKey)

	var secretKey []byte
	err = row.Scan(&secretKey)

	if err != nil {
		return nil, err
	}

	return secretKey, nil
}

func ValidateJWT(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Get the JWT token from the request
		rawToken := r.URL.Query().Get("jwt")

		if rawToken == "" {
			rawToken = r.Header.Get("Authorization")[len("JWT "):]
		}

		// Decode the JWT token to get the clientKey
		token, err := jwt.Parse(rawToken, func(token *jwt.Token) (interface{}, error) {
			// Don't forget to validate the alg is what you expect:
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			decodedClientKey := token.Claims["iss"].(string)
			return lookupSecret(decodedClientKey)
		})

		if err != nil {
			prettyErr := fmt.Errorf("JWT Validate : error on decoding %s : %s", rawToken, err.Error())
			log.Println(prettyErr.Error())
			http.Error(w, prettyErr.Error(), http.StatusInternalServerError)
			return
		}

		if !token.Valid {
			log.Println("JWT Validate : invalid token")
			http.Error(w, "JWT Validate : invalid token", http.StatusInternalServerError)
			return
		}

		fn(w, r, token.Claims["iss"].(string))
	}
}

func createQueryStringHash(r *http.Request) string {
	// TODO The Path needs more work to fit to spec for canonicalization
	return r.Method + "&" + r.URL.Path + "&" + r.URL.Query().Encode()
}

func GenerateClientsideJWTToken(clientKey string, r *http.Request) (string, error) {
	return generateJWT(clientKey, clientKey, r)
}

func GenerateJWTToken(clientKey string, r *http.Request) (string, error) {
	return generateJWT("git-ratchet-bucket", clientKey, r) // TODO config
}

func generateJWT(iss string, clientKey string, r *http.Request) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	token.Claims["iss"] = iss
	token.Claims["iat"] = time.Now().Unix()
	token.Claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	if r != nil {
		token.Claims["qsh"] = createQueryStringHash(r)
	}
	token.Claims["sub"] = clientKey

	secretKey, err := lookupSecret(clientKey)

	if err != nil {
		return "", err
	}

	tokenString, err := token.SignedString(secretKey)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func AddJWTAuthHeader(clientKey string, req *http.Request) error {
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	jwtToken, err := GenerateJWTToken(clientKey, req)

	if err != nil {
		return err
	}

	req.Header.Set("Authorization", "JWT "+jwtToken)

	return nil
}

func ConnectPost(clientKey string, url string, data string) (resp *http.Response, err error) {
	req, err := http.NewRequest("POST", url, strings.NewReader(data))

	if err != nil {
		return nil, err
	}

	if err = AddJWTAuthHeader(clientKey, req); err != nil {
		return nil, err
	}

	return http.DefaultClient.Do(req)
}

func ConnectGet(clientKey string, url string) (resp *http.Response, err error) {
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return nil, err
	}

	if err = AddJWTAuthHeader(clientKey, req); err != nil {
		return nil, err
	}

	return http.DefaultClient.Do(req)
}

func GetOauthAccessToken(clientKey string) (string, error) {
	var formData = "grant_type=urn:bitbucket:oauth2:jwt"

	resp, err := ConnectPost(clientKey, "https://bitbucket.org/site/oauth2/access_token", formData)

	if err != nil {
		return "", err
	}

	// Check the status code
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return "", fmt.Errorf("Non 2xx status code from endpoint. Code %d, Body: %s", resp.StatusCode, resp.Body)
	}

	decoder := json.NewDecoder(resp.Body)

	var oauth_data map[string]interface{}
	err = decoder.Decode(&oauth_data)

	if err != nil {
		return "", err
	}

	if _, hasToken := oauth_data["access_token"]; hasToken {
		return oauth_data["access_token"].(string), nil
	}

	return "", nil
}
