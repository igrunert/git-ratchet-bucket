package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"strconv"
	"strings"
)

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	err := healthCheck()
	if err != nil {
		log.Printf("Healtcheck failed: %s", err)
		http.Error(w, "Uh oh!", http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "No wuckas.")
}

const testRepo = "igrunert_atlassian/test-repo"

func healthCheck() error {
	// Check disk usage
	cmd := exec.Command("df", "/")

	outRaw, err := cmd.Output()

	out := bufio.NewScanner(bytes.NewReader(outRaw))
	out.Scan()
	out.Scan()
	data := out.Text()
	percentage := strings.Fields(data)[4]
	used, err := strconv.Atoi(percentage[:len(percentage)-1])

	if err != nil {
		return err
	}

	if used >= 95 {
		return fmt.Errorf("%s disk space used!", percentage)
	}

	// Check we can execute a simple git-ratchet command
	err = DumpRatchet(testRepo, "master", ioutil.Discard)
	if err != nil {
		return err
	}
	// Check we can talk to the DB
	_, err = GetRepo(testRepo, "")
	if err != nil {
		return err
	}

	return nil
}
