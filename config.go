package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"os"
)

type Environment struct {
	Dev  *Config
	Prod *Config
}

type Config struct {
	// Atlassian Connect JSON config
	Suffix      string
	ConsumerKey string

	// HTTP serving config
	Https    bool
	Address  string
	CertFile string
	KeyFile  string

	// Repo store config
	RepoStorePath        string
	MaxRepoSize          int
	MaxRepoSizeAtlassian int
	MaxConcurrentFetches int
	MaxEnqueuedFetches   int
	FetchTimeout         string // time.Duration

	// Database config
	Driver     string
	DataSource string
}

func ReadConfig(filename string, environment string) (*Config, error) {
	fmt.Println("Reading config from", filename)

	configFile, err := os.Open(filename)
	if err != nil {
		return nil, errors.New("Unable to open config file")
	}

	dec := json.NewDecoder(configFile)

	var e Environment
	if err := dec.Decode(&e); err != nil {
		return nil, err
	}

	switch environment {
	case "prod":
		return e.Prod, nil
	}
	return e.Dev, nil
}

func (c *Config) OpenDb() (*sql.DB, error) {
	return sql.Open(c.Driver, c.DataSource)
}
