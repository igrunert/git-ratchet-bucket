var meta = {};
['pathToken', 'commit', 'token'].forEach(function(name) {
    meta[name] = $('meta[name=' + name + ']').attr("content");
});

function pollServer() {
    $.ajax({
        method: 'GET',
        url: '/git-ratchet-bucket/status.json',
        dataType: 'json',
        data: {
            pathToken: meta.pathToken,
            commit: meta.commit
        },
        headers: {
            'Authorization': 'JWT ' + meta.token
        },
        success: function(data) {
            var $message = $('#message');
            var $spinner = $('#loading');
            switch (data.State) {
                case 3: // READY
                    $spinner.hide();

                    if (data.Dirty) {
                        $message.text('Git Ratchet Graphs is fetching the latest changes from your repository. The data ' +
                                      'below was last fetched at: ' + data.Updated);
                    } else {
                        $message.hide();
                    }
                    renderChart(data, meta.pathToken);
                    break;
                case 2: // FETCHING
                    $spinner.hide();
                    $message.text('Git Ratchet Graphs is fetching the latest changes from your repository. The data ' +
                                  'below was last fetched at: ' + data.Updated);
                    renderChart(data, meta.pathToken);
                    break;
                case 1: // CLONING
                    $message.text('Cloning repository..');
                    setTimeout(pollServer, 3000);
                    break;
                case 5: // ERROR
                    $spinner.hide();
                    $message.text('error: ' + data.Error);
                    break;
                case 4: // OVERSIZE
                    $spinner.hide();
                    $message.text('This repository is too large. Git Ratchet Graphs currently only supports repositories up ' +
                                  'to 25MB.');
                    break;
                default:
                    $spinner.hide();
                    $message.text('The repository is in an unknown state (' + data.State + ').');
                    break;
            }
        },
        error: function(res, status, err) {
            err = (res.responseText && JSON.parse(res.responseText).error) || err;
            $spinner.hide();
            $message.text(status + ' ' + err);
        }
    });
}

function renderChart(data, pathToken) {
    var chartWidth = 1024;
    var chartHeight = 600;
    var measure = data.Measures.split(",")[0]
    
    if (!measure) {
        $("#title").text("no git-ratchet measures found");
        return;
    }

    var chart = new c3.generate({
        bindto: '#chart',
        data: {
            x: "Timestamp",
            xFormat: '%Y-%m-%dT%H:%M:%S %Z',
            url: "/git-ratchet-bucket/data.csv?pathToken=" + pathToken + "&measure=" + measure + "&dummy=foo.csv"
        },
        size: {
            height: chartHeight
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: '%Y-%m-%d',
                    fit: false
                }
            },
            y: {
                min: 0,
                padding: {
                    top: 100,
                    bottom: 0
                }
            }
        }
    });
    
    $("#title").text("git-ratchet measures: " + measure);
    
    AP.resize(chartWidth, chartHeight);
}

pollServer();
