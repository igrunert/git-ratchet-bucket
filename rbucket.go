package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

const (
	mfest   = "atlassian-connect.json"
	rbucket = "ratchet-bucket.html"
)

var templates = template.Must(template.ParseFiles(mfest, rbucket))
var configFile = flag.String("configFile", "config.json", "Location of configuration json")
var environment = flag.String("environment", "dev", "Selected environment from configuration json")

type ManifestData struct {
	LocalBaseUrl string
	ConsumerKey  string
	Suffix       string
}

func manifest(config *Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := &ManifestData{
			LocalBaseUrl: r.Host,
			ConsumerKey:  config.ConsumerKey,
			Suffix:       config.Suffix,
		}
		err := templates.ExecuteTemplate(w, mfest, ctx)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

type RatchetData struct {
	LocalBaseUrl string
	Commit       string
	Token        string
	PathToken    string
}

func home(w http.ResponseWriter, r *http.Request, clientKey string) {
	token, err := GenerateClientsideJWTToken(clientKey, nil)

	if err != nil {
		prettyErr := fmt.Errorf("Home: Error generating jwt token : %s", err.Error())
		log.Printf(prettyErr.Error())
		http.Error(w, prettyErr.Error(), http.StatusInternalServerError)
	}

	var ctx = &RatchetData{
		LocalBaseUrl: r.Host,
		Commit:       r.URL.Query().Get("commit"),
		Token:        token,
		PathToken:    GetRepoToken(r.URL.Query().Get("repoPath")),
	}

	err = templates.ExecuteTemplate(w, rbucket, ctx)
	if err != nil {
		log.Printf("Home: Error executing template %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func webhook(w http.ResponseWriter, r *http.Request, clientKey string) {
	decoder := json.NewDecoder(r.Body)

	var webhook_data map[string]interface{}
	err := decoder.Decode(&webhook_data)

	if err != nil {
		log.Println("Webhook : Error processing %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, hasData := webhook_data["data"]; hasData {
		webhook_data_block := webhook_data["data"].(map[string]interface{})
		if _, hasRepo := webhook_data_block["repository"]; hasRepo {
			slug := (webhook_data_block["repository"].(map[string]interface{}))["full_name"].(string)
			log.Printf("Fetch %s : webhook received", slug)

			QueueFetch(slug, clientKey)
		}
	}
}

func status(w http.ResponseWriter, r *http.Request, clientKey string) {
	repo, err := GetRepo(GetSlugFromToken(r.URL.Query().Get("pathToken")), clientKey)

	if err != nil {
		prettyErr := fmt.Errorf("Status : Error getting repo %s", err.Error())
		log.Println(prettyErr.Error())
		http.Error(w, prettyErr.Error(), http.StatusInternalServerError)
		return
	}

	enc := json.NewEncoder(w)

	enc.Encode(&repo)
}

func data(w http.ResponseWriter, r *http.Request) {
	repo, err := GetRepo(GetSlugFromToken(r.URL.Query().Get("pathToken")), "")

	if err != nil {
		prettyErr := fmt.Errorf("Ratchet : Error getting repo.")
		log.Println(prettyErr.Error())
		http.Error(w, prettyErr.Error(), http.StatusInternalServerError)
		return
	}

	if checkLastModified(w, r, repo.Updated) {
		return
	}

	err = DumpRatchet(repo.Slug, r.URL.Query().Get("measure"), w)

	if err != nil {
		prettyErr := fmt.Errorf("Ratchet %s : Error dumping data.", repo.Slug)
		log.Println(prettyErr.Error())
		http.Error(w, prettyErr.Error(), http.StatusInternalServerError)
	}
}

func checkLastModified(w http.ResponseWriter, r *http.Request, modtime time.Time) bool {
	if modtime.IsZero() {
		return false
	}

	// The Date-Modified header truncates sub-second precision, so
	// use mtime < t+1s instead of mtime <= t to check for unmodified.
	if t, err := time.Parse(http.TimeFormat, r.Header.Get("If-Modified-Since")); err == nil && modtime.Before(t.Add(1*time.Second)) {
		h := w.Header()
		delete(h, "Content-Type")
		delete(h, "Content-Length")
		w.WriteHeader(http.StatusNotModified)
		return true
	}
	w.Header().Set("Last-Modified", modtime.UTC().Format(http.TimeFormat))
	w.Header().Set("Cache-Control", "private, must-revalidate")
	return false
}

func main() {
	flag.Parse()

	config, err := ReadConfig(*configFile, *environment)
	if err != nil {
		log.Fatalf(err.Error())
	}

	if err = InitRepoStore(*config); err != nil {
		log.Fatalf(err.Error())
	}

	if err = InitConnectStorage(config.OpenDb); err != nil {
		log.Fatalf(err.Error())
	}

	http.HandleFunc("/installed", AddonInstalled)
	http.HandleFunc("/"+mfest, manifest(config))
	http.HandleFunc("/git-ratchet-bucket", ValidateJWT(home))
	http.HandleFunc("/git-ratchet-bucket/status.json", ValidateJWT(status))
	http.HandleFunc("/git-ratchet-bucket/data.csv", data)
	http.HandleFunc("/webhook", ValidateJWT(webhook))
	http.HandleFunc("/healthcheck", HealthCheck)
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	log.Println("Ready to serve at", config.Address)
	if config.Https {
		tlsconfig := &tls.Config{MinVersion: tls.VersionTLS10}
		server := &http.Server{Addr: config.Address, Handler: nil, TLSConfig: tlsconfig}

		err = server.ListenAndServeTLS(config.CertFile, config.KeyFile)
	} else {
		err = http.ListenAndServe(config.Address, nil)
	}
	if err != nil {
		log.Fatalf(err.Error())
	}
}
