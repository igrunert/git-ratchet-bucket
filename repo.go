// This manages the cache of repos for the server.

// Cloning - getting copies of the remote repos.
// Limits - need to live within server storage constraints, network constraints, timeout requests.
// Eviction - clean up repos not requested in a given time period.
// Freshness - respond to webhooks and keep repos up to date (or mark them as dirty).
// Health checks - can validate the health of the repo storage.

// It should be restartable and re-use the repo storage. This means storing metadata next to the repos.

// The fetch queue will be serviced by a configurable number of goroutines.
package main

import (
	"crypto/rand"
	"database/sql"
	"encoding/csv"
	"encoding/json"
	"fmt"
	ratchet "github.com/iangrunert/git-ratchet/store"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"time"
)

type RepoState int

const (
	Unknown  RepoState = iota // 0
	Cloning                   // 1
	Fetching                  // 2
	Ready                     // 3
	Oversize                  // 4
	Error                     // 5
)

type Repo struct {
	Slug     string
	Error    string
	State    RepoState
	Updated  time.Time
	Dirty    bool
	Measures string
}

const repoSchema = `
create table if not exists repository(
  slug text,
  error text,
  state int not null,
  updated text,
  dirty boolean,
  measures text
);`

type FetchRequest struct {
	slug      string
	clientKey string
}

var queue chan *FetchRequest
var storedir string
var fetchTimeout time.Duration

var openRepoDb func() (*sql.DB, error)

func InitRepoStore(config Config) error {
	log.Println("Initialising git storage...")

	storedir = config.RepoStorePath

	if !directoryExists(storedir) {
		return fmt.Errorf("Invalid directory given for repository storage!")
	}

	var err error
	fetchTimeout, err = time.ParseDuration(config.FetchTimeout)

	if err != nil {
		return fmt.Errorf("Invalid format for fetch timeout, %s", err.Error())
	}

	queue = make(chan *FetchRequest, config.MaxEnqueuedFetches)

	// Start the goroutines for the fetch queue
	for i := 0; i < config.MaxConcurrentFetches; i++ {
		go handle(queue, config)
	}

	log.Println(storedir, "has been initialised for git storage.")
	log.Println(config.MaxConcurrentFetches, "goroutines started to service fetch queue.")
	log.Println(config.MaxEnqueuedFetches, "is the limit of enqueued fetches.")
	log.Println(config.FetchTimeout, "is the time limit on fetches.")

	openRepoDb = config.OpenDb

	db, err := openRepoDb()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(repoSchema)
	return err
}

func GetRepo(slug string, clientKey string) (*Repo, error) {
	db, err := openRepoDb()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	// Get the latest repo data
	row := db.QueryRow("select * from repository where slug = ? order by datetime(updated) desc limit 1", slug)

	var repo Repo
	// Need to parse updated from it's string representation
	var updatedRaw string

	err = row.Scan(&repo.Slug, &repo.Error, &repo.State, &updatedRaw, &repo.Dirty, &repo.Measures)
	if err == sql.ErrNoRows && clientKey != "" {
		return queueUpdate(db, slug, clientKey, Cloning)
	}

	if err != nil {
		return nil, err
	}

	updated, err := time.Parse(time.RFC3339, updatedRaw)

	if err != nil {
		return nil, err
	}

	repo.Updated = updated

	if repo.Dirty && time.Since(updated).Minutes() > 5 && clientKey != "" {
		log.Printf("Fetch %s : Repo still dirty after 5 minutes, queueing again", repo.Slug)
		return QueueFetch(slug, clientKey)
	}

	return &repo, nil
}

func DumpRatchet(slug string, prefix string, writer io.Writer) error {
	repoDir := path.Join(storedir, slug, "/")

	gitlog := exec.Command("git", "-C", repoDir, "--no-pager", "log", "--show-notes=git-ratchet-1-"+prefix, `--pretty=format:'%H,%an <%ae>,%at,"%N",'`, "HEAD")

	log.Printf("Ratchet %s : executing git command %s", slug, strings.Join(gitlog.Args, " "))

	readStoredMeasure, err := ratchet.CommitMeasures(gitlog)

	if err != nil {
		return err
	}

	writtenHeader := false

	for {
		cm, err := readStoredMeasure()

		// Empty state of the repository - no stored metrics.
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		out := csv.NewWriter(writer)

		if !writtenHeader {
			headers := []string{"Timestamp"}
			for _, measure := range cm.Measures {
				if slug == "igrunert_atlassian/confluence-mirror" {
					if strings.Index(measure.Name, "gzip") == -1 {
						headers = append(headers, measure.Name)
					}
				} else {
					headers = append(headers, measure.Name)
				}
			}

			out.Write(headers)
			writtenHeader = true
		}

		values := []string{cm.Timestamp.Format("2006-01-02T15:04:05 -0700")}
		for _, measure := range cm.Measures {
			if slug == "igrunert_atlassian/confluence-mirror" {
				if strings.Index(measure.Name, "gzip") == -1 {
					values = append(values, strconv.Itoa(measure.Value/1024))
				}
			} else {
				values = append(values, strconv.Itoa(measure.Value))
			}
		}
		out.Write(values)
		out.Flush()
	}

	done := make(chan error)
	go func() { done <- gitlog.Wait() }()
	select {
	case <-done:
		log.Printf("Ratchet %s : finished executing git command %s", slug, strings.Join(gitlog.Args, " "))
		return nil
	case <-time.After(time.Second):
		log.Printf("Ratchet %s : timed out executing git command %s", slug, strings.Join(gitlog.Args, " "))
		if err := gitlog.Process.Kill(); err != nil {
			prettyErr := fmt.Errorf("Ratchet %s : failed to kill git command %s : %s", slug, strings.Join(gitlog.Args, " "), err)
			log.Println(prettyErr.Error())
			return err
		}
		<-done // allow goroutine to exit
	}

	return nil
}

func QueueFetch(slug string, clientKey string) (*Repo, error) {
	db, err := openRepoDb()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	return queueUpdate(db, slug, clientKey, Fetching)
}

func queueUpdate(db *sql.DB, slug string, clientKey string, state RepoState) (*Repo, error) {
	// TODO Possible race condition here
	if len(queue) == cap(queue) {
		return nil, fmt.Errorf("Fetch %s : Cannot enqueue - server over capacity.", slug)
	}
	// TODO There's a potential race condition here, which could cause a double insert.
	// Should be using transactions here to prevent this.
	repo := Repo{
		Slug:    slug,
		Error:   "",
		State:   state,
		Updated: time.Now(),
		Dirty:   true,
	}

	_, err := db.Exec("insert into repository(slug, error, state, updated, dirty, measures) values (?, ?, ?, ?, ?, ?)", repo.Slug, repo.Error, repo.State, repo.Updated.Format(time.RFC3339), repo.Dirty, "")

	queue <- &FetchRequest{
		slug:      slug,
		clientKey: clientKey,
	}

	log.Printf("Fetch %s : queued fetch request", slug)

	return &repo, err
}

func handle(queue chan *FetchRequest, config Config) {
	for r := range queue {
		fetch(r, config)
	}
}

func fetch(request *FetchRequest, config Config) {
	log.Printf("Fetch %s : servicing fetch request", request.slug)
	// Check the size of the repository
	err := checkSize(request, config)

	if err == ErrorOversize {
		setState(request.slug, "", Oversize)
		return
	}

	if err != nil {
		setError(request.slug, err)
		return
	}

	repoDir := path.Join(storedir, request.slug, "/")

	log.Printf("Fetch %s : size check passed", request.slug)
	// Ensure a directory exists to store the repository
	err = os.MkdirAll(repoDir, 0777)

	if err != nil {
		setError(request.slug, err)
		return
	}
	log.Printf("Fetch %s : directory created %s", request.slug, repoDir)

	oauthToken, err := GetOauthAccessToken(request.clientKey)

	if err != nil {
		setError(request.slug, err)
		return
	}

	log.Printf("Fetch %s : oauth token received", request.slug)

	cloneUrl := fmt.Sprintf("https://x-token-auth:%s@bitbucket.org/%s.git", oauthToken, request.slug)

	// If the directory is empty git clone, otherwise git fetch
	var gitCmds []*exec.Cmd

	refs := path.Join(repoDir, "refs")
	log.Printf("Fetch %s : directory %s is there %s", request.slug, refs, directoryExists(refs))
	if directoryExists(refs) {
		gitCmds = []*exec.Cmd{exec.Command("git", "-C", repoDir, "fetch", cloneUrl, "refs/heads/*:refs/heads/*", "refs/notes/*:refs/notes/*")}
	} else {
		gitCmds = []*exec.Cmd{
			exec.Command("git", "clone", "--bare", cloneUrl, repoDir),
			exec.Command("git", "-C", repoDir, "fetch", cloneUrl, "refs/heads/*:refs/heads/*", "refs/notes/*:refs/notes/*"),
		}
	}

	for _, gitCmd := range gitCmds {
		log.Printf("Fetch %s : executing git command %s", request.slug, strings.Join(gitCmd.Args, " "))
		// Wait until fetch timeout expires or the command completes, whatever comes first
		if err := gitCmd.Start(); err != nil {
			setError(request.slug, err)
			return
		}
		done := make(chan error)
		go func() { done <- gitCmd.Wait() }()
		select {
		case err := <-done:
			log.Printf("Fetch %s : finished executing git command %s", request.slug, strings.Join(gitCmd.Args, " "))
			// exited
			if err != nil {
				gitFail := fmt.Errorf("Fetch %s : error executing git command %s : %s", request.slug, strings.Join(gitCmd.Args, " "), err.Error())
				log.Printf(gitFail.Error())
				setError(request.slug, gitFail)
				return
			}
		case <-time.After(fetchTimeout):
			timeout := fmt.Errorf("Fetch %s : timed out executing git command %s", request.slug, strings.Join(gitCmd.Args, " "))
			gitCmd.Process.Kill()
			<-done // allow goroutine to exit
			log.Println(timeout.Error())
			// timed out
			setError(request.slug, timeout)
		}
	}

	log.Printf("Fetch %s : finished updating repository", request.slug)

	prefixes := []string{}

	notes, err := ioutil.ReadDir(path.Join(refs, "notes"))
	if err == nil {
		for _, f := range notes {
			if !f.IsDir() && strings.HasPrefix(f.Name(), "git-ratchet-1-") {
				prefixes = append(prefixes, f.Name()[len("git-ratchet-1-"):])
			}
		}
	}

	setState(request.slug, strings.Join(prefixes, ","), Ready)
}

func setState(slug string, measures string, state RepoState) {
	repo := Repo{
		Slug:     slug,
		Error:    "",
		State:    state,
		Updated:  time.Now(),
		Dirty:    false,
		Measures: measures,
	}

	storeRepoActivity(repo)
}

func setError(slug string, err error) {
	log.Printf("Error when performing fetch request for %s, %s", slug, err.Error())

	repo := Repo{
		Slug:     slug,
		Error:    err.Error(),
		State:    Error,
		Updated:  time.Now(),
		Dirty:    false,
		Measures: "",
	}

	storeRepoActivity(repo)
}

func storeRepoActivity(repo Repo) {
	db, err := openRepoDb()
	if err != nil {
		log.Printf("Error opening db connection to store repo activity, %s", err.Error())
		return
	}
	defer db.Close()

	_, err = db.Exec("insert into repository(slug, error, state, updated, dirty, measures) values (?, ?, ?, ?, ?, ?)", repo.Slug, repo.Error, repo.State, repo.Updated.Format(time.RFC3339), repo.Dirty, repo.Measures)

	if err != nil {
		log.Printf("Error storing repo activity, %s", err.Error())
	}
}

var ErrorOversize = fmt.Errorf("Repository is over the maximum size limit.")

func checkSize(request *FetchRequest, config Config) error {
	resp, err := ConnectGet(request.clientKey, "https://api.bitbucket.org/2.0/repositories/"+request.slug)

	if err != nil {
		return fmt.Errorf("Error when getting size of repository. Error: %s", err.Error())
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("%d status code when getting size of repository.", resp.StatusCode)
	}

	decoder := json.NewDecoder(resp.Body)

	var size_data map[string]interface{}
	err = decoder.Decode(&size_data)

	if err != nil {
		return fmt.Errorf("Couldn't parse response when querying repo size. Error: %s", err.Error())
	}

	if _, hasSize := size_data["size"]; hasSize {
		size := int(size_data["size"].(float64))

		if size < config.MaxRepoSize {
			return nil
		}

		if (strings.Index(request.slug, "atlassian") == 0 || strings.Index(request.slug, "igrunert_atlassian") == 0) && size < config.MaxRepoSizeAtlassian {
			return nil
		}

		return ErrorOversize
	}

	return fmt.Errorf("Couldn't find data in response when querying repo size.")
}

func directoryExists(path string) bool {
	fileInfo, err := os.Stat(path)
	if err == nil && fileInfo.IsDir() {
		return true
	}
	return false
}

var repoTokens = map[string]string{}
var repoPaths = map[string]string{}

func GetRepoToken(slug string) string {
	token, there := repoTokens[slug]

	if !there {
		token = randString(64)
		repoTokens[slug] = token
		repoPaths[token] = slug
	}

	return token
}

func GetSlugFromToken(token string) string {
	return repoPaths[token]
}

func randString(n int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}
